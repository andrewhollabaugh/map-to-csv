# map-to-csv

Python script to analyze C/C++ linker map files and generate .csv outputs.

Usage:
python map-to-csv.py [map file]

Generates a folder with a .csv for .text, .bss, and .data
