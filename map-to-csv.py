#!/usr/bin/python

import sys
import os
import csv

def get_map_file_list(map_file_path):
    map_file_list = []
    with open(map_file_path, "r") as map_file:
        map_file_list = map_file.readlines()
    return map_file_list

def find_section_header(map_file_list, section_title):
    line = ""
    line_count = 0

    while line[0:len(section_title)] != section_title:
        line = map_file_list[line_count]
        line_count += 1

    return line_count

def get_size_from_line(line):
    size_pos_end = 44
    size_pos_start = size_pos_end

    while line[size_pos_start] != 'x':
        size_pos_start -= 1

    return str(int(line[size_pos_start + 1 : size_pos_end + 1], base=16))

def get_symbol_data(map_file_list, mem_type, start_line):
    symbol_data = []
    total_size = 0;

    for line in map_file_list[start_line:]: #only check lines after the memory map header
        if line[1:len(mem_type)+1] == mem_type: #find lines with the matching mem_type
            symbol_data_inst = []

            next_line = map_file_list[map_file_list.index(line) + 1]
            next_next_line = map_file_list[map_file_list.index(line) + 2]

            if "0x000" in line: #is the line with the actual data on the same or the next line as the mem_type
                symbol_data_inst.append(get_size_from_line(line)) #size
                symbol_data_inst.append(next_line[50:].rstrip('\n')) #symbol name
                symbol_data_inst.append(line[46:].rstrip('\n')) #object name
            else:
                symbol_data_inst.append(get_size_from_line(next_line)) #size
                symbol_data_inst.append(next_next_line[50:].rstrip('\n')) #symbol name
                symbol_data_inst.append(next_line[46:].rstrip('\n')) #object name

            total_size += int(symbol_data_inst[0])
            symbol_data.append(symbol_data_inst)

    total_size_data_inst = [total_size, "total size"]
    symbol_data.insert(0, total_size_data_inst)

    return symbol_data

def get_object_data(symbol_data):
    object_data = []
    total_size = 0

    for symbol_data_inst in symbol_data[1:]:

        #check if object is already in object_data
        is_already_in = False
        already_in_index = 0
        for object_data_inst in object_data:
            if object_data_inst.count(symbol_data_inst[2]) > 0:
                is_already_in = True
                already_in_index = object_data.index(object_data_inst)
                
        if is_already_in:#add the size of the current symbol to the total object size
            new_size = int(object_data[already_in_index][0]) + int(symbol_data_inst[0]) 
            object_data[already_in_index][0] = str(new_size)
        else:
            object_data_inst = []
            object_data_inst.append(symbol_data_inst[0])
            object_data_inst.append(symbol_data_inst[2])
            object_data.append(object_data_inst)

    for object_data_inst in object_data:
        total_size += int(object_data_inst[0])

    total_size_data_inst = [total_size, "total size"]
    object_data.insert(0, total_size_data_inst)

    return object_data

def sort_data(data):
    for data_inst in data:
        data_inst[0] = int(data_inst[0])

    return sorted(data, key=lambda l:l[0], reverse=True)

def print_data(data):
    for data_inst in data:
        print(data_inst)

def write_data_to_csv(value_list, file_path):
    with open(file_path, "a") as csv_file:
        csv_writer = csv.writer(csv_file, delimiter=',')
        for value in value_list:
            csv_writer.writerow(value)

def write_str_to_csv(string, file_path):
    with open(file_path, "a") as csv_file:
        csv_writer = csv.writer(csv_file, delimiter=',')
        csv_writer.writerow([string])

def compose_csv_output(symbol_data, object_data, csv_file_path):
    write_str_to_csv("SORTED BY SIZE: OBJECTS", csv_file_path)
    write_data_to_csv(sort_data(object_data), csv_file_path)
    write_str_to_csv(" ", csv_file_path)
    write_str_to_csv(" ", csv_file_path)
    write_str_to_csv("SORTED BY SIZE: SYMBOLS", csv_file_path)
    write_data_to_csv(sort_data(symbol_data), csv_file_path)
    write_str_to_csv(" ", csv_file_path)
    write_str_to_csv(" ", csv_file_path)
    write_str_to_csv(" ", csv_file_path)
    write_str_to_csv(" ", csv_file_path)
    write_str_to_csv("UNSORTED: OBJECTS", csv_file_path)
    write_data_to_csv(object_data, csv_file_path)
    write_str_to_csv(" ", csv_file_path)
    write_str_to_csv(" ", csv_file_path)
    write_str_to_csv("UNSORTED: SYMBOLS", csv_file_path)
    write_data_to_csv(symbol_data, csv_file_path)

#===========================================================

try:
    map_file_path = sys.argv[1]
except:
    print("error: need to specify map file location")
    quit()

output_folder_name = "map-to-csv-out"

try:
    os.mkdir(output_folder_name)
except:
    pass

mem_section_title = "Linker script and memory map"
map_file_list = get_map_file_list(map_file_path)
section_header_line = find_section_header(map_file_list, mem_section_title)

mem_type_list = [ \
        [".text", "text.csv"], \
        [".bss", "bss.csv"], \
        [".data", "data.csv"]]

for mem_type_inst in mem_type_list:
    mem_type = mem_type_inst[0]
    csv_file_path = output_folder_name + "/" + mem_type_inst[1]

    symbol_data = get_symbol_data(map_file_list, mem_type, section_header_line)
    object_data = get_object_data(symbol_data)

    if os.path.isfile(csv_file_path):
        os.remove(csv_file_path)

    compose_csv_output(symbol_data, object_data, csv_file_path)

